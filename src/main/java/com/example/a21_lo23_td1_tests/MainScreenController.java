package com.example.a21_lo23_td1_tests;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

public class MainScreenController {

    @FXML
    private GridPane gameBoard ;

    @FXML
    private Label label_1 ;

    @FXML
    private TextField chatTextField ;

    @FXML
    private TextArea chatTextArea ;

    @FXML
    private Pane testPane ;

    @FXML
    public void initGameBoard() {
        for (int i = 0 ; i < 13 ; i++) {
            for (int j = 0 ; j < 13 ; j++) {
                Pane p = new Pane() ;
                gameBoard.add(p, i, j) ;
            }
        }
    }

    @FXML
    private void placeTower(MouseEvent event) {
        Node source = (Node) event.getTarget() ;
        Integer colIndex = GridPane.getColumnIndex(source) ;
        Integer rowIndex = GridPane.getRowIndex(source) ;
        System.out.println("test : " + colIndex + ", " + rowIndex);
        label_1.setText("(" + colIndex + ", " + rowIndex + ")") ;
    }

    @FXML
    private void sendMessage(MouseEvent event) {
        String chatTextAreaContent = chatTextArea.getText(), chatTextFieldContent = chatTextField.getText() ;
        chatTextArea.setText(chatTextAreaContent + "\n" + chatTextFieldContent) ;
    }

    @FXML
    private void colorizePane(MouseEvent event) {
        testPane.setStyle("-fx-background-color: #726a6a;");
    }

    @FXML
    private void resetPaneColor(MouseEvent event) {
        testPane.setStyle("-fx-background-color: #ffffff;");
    }

}