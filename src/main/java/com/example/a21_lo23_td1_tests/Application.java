package com.example.a21_lo23_td1_tests;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Application extends javafx.application.Application {

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource("main_screen.fxml"));
        Parent root = (Parent) fxmlLoader.load();
        MainScreenController c = fxmlLoader.getController() ;
        c.initGameBoard() ;
        Scene scene = new Scene(root, 660, 580);
        stage.setTitle("NOMAD");
        stage.setScene(scene);
        stage.show();
    }



    public static void main(String[] args) {
        launch();
    }
}